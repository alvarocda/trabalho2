sshpass -e ssh -o StrictHostKeyChecking=no root@$HOST_DEV << ENDSSH
  docker network create container-lan || true
  echo "Baixando Imagem do MySQL"
  docker pull $MYSQL_IMAGE
  docker rm --force mysql || true
  echo "Iniciando container MySQL"
  docker run -d -it -p 3306:3306 --network container-lan -e MYSQL_ROOT_PASSWORD=root --name mysql $MYSQL_IMAGE



  echo "Baixando Imagem do phpMyAdmin"
  docker pull $PHPMYADMIN_IMAGE
  docker rm --force phpMyAdmin || true
  echo "Iniciando container phpMyAdmin"
  docker run -d -it -p 8888:80 --network container-lan -e MYSQL_ROOT_PASSWORD=root --name phpmyadmin $PHPMYADMIN_IMAGE
  docker system prune -f
ENDSSH
